// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';
const WELCOMING_FAREWELL = "Give me a yell or say hi if you want to do anything else"
const functions = require('firebase-functions');
const {
    WebhookClient
} = require('dialogflow-fulfillment');
const {
    Card,
    Suggestion
} = require('dialogflow-fulfillment');

process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements

exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
    const agent = new WebhookClient({
        request,
        response
    });

    function fallback(agent) {
        agent.add(`I didn't understand`);
        agent.add(`I'm sorry, can you try again?`);
    }

    function joke(agent) {
        var jokes = [
            "Why does Australia need a lot of skilled plumbers? Because it is surrounded by water.",
            "A plumber is the only person who can take a leak while they fix a leak!",
            "Do you know how a doctor and plumber are both alike? They both bury their mistakes.",
            "What did one toilet say to the other toilet? You look flushed!",
            "Q: How many plumbers does it take to screw in a light bulb? A:One to get the beer and one to call the electrician.",
            "Q: What’s the main difference between an electrician and a plumber? A: An electrician washes his hands AFTER he has gone potty, but a plumber washes his hands BEFORE he goes potty.",
            "Q: What is the definition of disgusting? A: Seeing a plumber bite his nails.",
            "Q: Why shouldn't you play poker with a plumber? A: A good flush beats a full house every time.",
            `A plumber was called to a doctors home to fix leaking faucet that had kept the surgeon awake late at night. After a two-minute job the plumber demanded $150. The surgeon exclaimed, 'I don't charge this amount even though I am a surgeon." The plumber replied, "I agree, you are right. I too, didn't either, when I was a surgeon. That's why I switched to plumbing!`
        ];

        agent.add(jokes[Math.floor(Math.random() * 100) % jokes.length]);
        agent.add("another joke?");
        agent.add(new Suggestion('Yes'));
        agent.add(new Suggestion('No'));
    }

    function triggerMenuEvent(agent) {
        agent.add(WELCOMING_FAREWELL);
    }

    function search(agent) {
        var searchQuery = request.body.queryResult.parameters.any;
        agent.add( new Card({
            title: `Search results for ` + searchQuery,
            text: `Results Page`,
            buttonText: 'Click here',
            buttonUrl: 'https://www.reece.com.au/bathrooms/products/?keyword=' + searchQuery
        }));
        agent.add(WELCOMING_FAREWELL)
    }

    let intentMap = new Map();
    intentMap.set('Default Fallback Intent', fallback);
    intentMap.set('jokes.get', joke);
    intentMap.set('jokes.get.more', joke);
    intentMap.set('actions.intent.GET_JOKE', joke);
    intentMap.set('jokes.feedback.bad', triggerMenuEvent);
    intentMap.set("search", search);

    agent.handleRequest(intentMap);
});